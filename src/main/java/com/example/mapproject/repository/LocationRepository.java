package com.example.mapproject.repository;

import com.example.mapproject.entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {

    Location findLocationById(Integer id);
}