package com.example.mapproject.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity
@Table(name = "LOCATION")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Column(name = "City", length = 64, nullable = false)
    private String city;

    @Column(name = "Long_Value", length = 20, nullable = false)
    private String longValue;

    @Column(name = "Lat_Value", length = 20, nullable = false)
    private String latValue;

    @Column(name = "Address", length = 250, nullable = false)
    private String address;

    @Column(name = "Address_Deatails", length = 250, nullable = false)
    private String addressDeatails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLongValue() {
        return longValue;
    }

    public void setLongValue(String longValue) {
        this.longValue = longValue;
    }

    public String getLatValue() {
        return latValue;
    }

    public void setLatValue(String latValue) {
        this.latValue = latValue;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddressDeatails() {
        return addressDeatails;
    }

    public void setAddressDeatails(String addressDeatails) {
        this.addressDeatails = addressDeatails;
    }
 }