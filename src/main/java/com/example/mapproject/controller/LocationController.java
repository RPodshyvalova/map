package com.example.mapproject.controller;

import com.example.mapproject.entity.Location;
import com.example.mapproject.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    @Autowired
    private LocationRepository locationRepository;

    @PostMapping("/add")
    public String addLocation(@RequestParam String city, @RequestParam String longValue, @RequestParam String latValue, @RequestParam String address, @RequestParam String addressDeatails) {
        Location location = new Location();
        location.setCity(city);
        location.setLongValue(longValue);
        location.setLatValue(latValue);
        location.setAddress(address);
        location.setAddressDeatails(addressDeatails);
        locationRepository.save(location);

        return "Added new location to repository";
    }

    @GetMapping("/list")
    public Iterable<Location> getLocations() {
        return locationRepository.findAll();
    }

    @GetMapping("/find/{id}")
    public Location findLocationById(@PathVariable Integer id) {
        return locationRepository.findLocationById(id);
    }
}
